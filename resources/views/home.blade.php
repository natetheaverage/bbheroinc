@extends('spark::layouts.app')

@section('content')
<home :user="user" inline-template>
      <section id="content">
                <div class="container container-alt">

                    <div class="row wall">
                        <div class="col-md-8">
                            <div class="card w-post">
                                <div class="card-body">
                                    <textarea class="wp-text auto-size" placeholder="Write Something..."></textarea>

                                    <div class="tab-content p-0">
                                        <div class="wp-media tab-pane" id="wpm-image">
                                            Images - Coming soon...
                                        </div>

                                        <div class="wp-media tab-pane" id="wpm-video">
                                            Video Links - Coming soon...
                                        </div>

                                        <div class="wp-media tab-pane" id="wpm-link">
                                            Links - Coming soon...
                                        </div>
                                    </div>

                                    <div class="wp-actions clearfix">
                                        <div class="wpa-media-list pull-left">
                                            <a data-toggle="tab" href="#wpm-image" class="c-amber">
                                                <i class="zmdi zmdi-image"></i>
                                            </a>
                                            <a data-toggle="tab" href="#wpm-video" class="c-green">
                                                <i class="zmdi zmdi-play-circle"></i>
                                            </a>
                                            <a data-toggle="tab" href="#wpm-link" class="c-blue">
                                                <i class="zmdi zmdi-link"></i>
                                            </a>
                                        </div>

                                        <button class="btn btn-primary btn-sm pull-right">Post</button>
                                    </div>
                                </div>
                            </div>

                            <div class="card w-item">
                                <div class="card-header">
                                    <div class="media">
                                        <div class="pull-left">
                                            <img class="avatar-img" src="img/profile-pics/1.jpg" alt="">
                                        </div>

                                        <div class="media-body">
                                            <h2>
                                            @{{ user.name }}
                                                <small>Posted on 31st Aug 2015 at 07:00</small>
                                            </h2>
                                        </div>
                                    </div>
                                </div>

                                <div class="card-body card-padding">
                                    <p>Interdum et malesuada fames ac ante ipsum primis in faucibus. Fusce eget dolor id justo luctus commodo vel pharetra nisi. Donec velit libero, gravida vel diam ut, lobortis mollis quam. Morbi posuere egestas posuere. Curabitur in dui sollicitudin, lacinia magna at, laoreet sapien. Integer vitae eros nulla.</p>

                                    <div class="wi-preview lightbox clearfix">
                                        <div class="wip-item" data-src="img/headers/4.png"
                                             style="background-image: url(img/headers/4.png);">
                                            <div class="lightbox-item"></div>
                                        </div>
                                    </div>

                                    <div class="wi-stats clearfix">
                                        <div class="wis-numbers">
                                            <span><i class="zmdi zmdi-comments"></i> 36</span>
                                            <span class="active"><i class="zmdi zmdi-favorite"></i> 220</span>
                                        </div>

                                        <div class="wis-commentors">
                                            <a href=""><img src="img/profile-pics/1.jpg" alt=""></a>
                                            <a href=""><img src="img/profile-pics/2.jpg" alt=""></a>
                                            <a href=""><img src="img/profile-pics/3.jpg" alt=""></a>
                                            <a href=""><img src="img/profile-pics/5.jpg" alt=""></a>
                                        </div>
                                    </div>
                                </div>

                                <div class="wi-comments">

                                    <!-- Comment Listing -->
                                    <div class="list-group">
                                        <div class="list-group-item media">
                                            <a href="" class="pull-left">
                                                <img src="img/profile-pics/5.jpg" alt="" class="lgi-img">
                                            </a>

                                            <div class="media-body">
                                                <a href="" class="lgi-heading">David Nathan <small class="c-gray m-l-10">3 mins ago...</small></a>

                                                <small class="lgi-text">Maecenas dignissim in neque id commodo. Nam pretium a tortor a convallis. Curabitur in arcu quis nulla aliquam condimentum. Morbi eu cursus diam, vitae tristique dui.</small>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Comment form -->
                                    <div class="wic-form">
                                        <textarea placeholder="Write something..." data-ma-action="wall-comment-open"></textarea>

                                        <div class="wicf-actions text-right">
                                            <button class="btn btn-sm btn-link" data-ma-action="wall-comment-close">Cancel</button>
                                            <button class="btn btn-sm btn-primary">Post</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="card w-item">
                                <div class="card-header">
                                    <div class="media">
                                        <div class="pull-left">
                                            <img class="avatar-img" src="img/profile-pics/3.jpg" alt="">
                                        </div>

                                        <div class="media-body">
                                            <h2>Felix Harper
                                                <small>Posted on 29th Aug 2015 at 02:10</small>
                                            </h2>
                                        </div>
                                    </div>
                                </div>

                                <div class="card-body card-padding">
                                    <p>Aliquam vel sem ut ligula posuere viverra. Ut dolor nisi, fringilla quis consectetur maximus, finibus vel diam. Duis condimentum, diam in semper congue, tortor orci condimentum urna</p>

                                    <div class="wi-stats clearfix">
                                        <div class="wis-numbers">
                                            <span><i class="zmdi zmdi-comments"></i> 0</span>
                                            <span><i class="zmdi zmdi-favorite"></i> 0</span>
                                        </div>

                                        <div class="wis-commentors">
                                            <a href=""><img src="img/profile-pics/3.jpg" alt=""></a>
                                        </div>
                                    </div>
                                </div>

                                <div class="wi-comments">
                                    <!-- Comment form -->
                                    <div class="wic-form">
                                        <textarea placeholder="Write something..." data-ma-action="wall-comment-open"></textarea>

                                        <div class="wicf-actions text-right">
                                            <button class="btn btn-sm btn-link" data-ma-action="wall-comment-close">Cancel</button>
                                            <button class="btn btn-sm btn-primary">Post</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="card w-item">
                                <div class="card-header">
                                    <div class="media">
                                        <div class="pull-left">
                                            <img class="avatar-img" src="img/profile-pics/2.jpg" alt="">
                                        </div>

                                        <div class="media-body">
                                            <h2>Morgan Francis
                                                <small>Posted on 2nd Sep 2015 at 17:00</small>
                                            </h2>
                                        </div>
                                    </div>
                                </div>

                                <div class="card-body card-padding">
                                    <p>Donec velit libero, gravida vel diam ut, lobortis mollis quam. Morbi posuere egestas posuere. Curabitur in dui sollicitudin, lacinia magna at, laoreetsapien. Integer vitae eros nulla.</p>

                                    <div class="wi-preview lightbox clearfix">
                                        <div class="wip-item" data-src="media/gallery/2.jpg"
                                             style="background-image: url(media/gallery/2.jpg);">
                                            <img src="media/gallery/thumbs/2.jpg" alt="">
                                        </div>
                                        <div class="wip-item" data-src="media/gallery/3.jpg"
                                             style="background-image: url(media/gallery/3.jpg);">
                                            <img src="media/gallery/thumbs/2.jpg" alt="">
                                        </div>
                                    </div>

                                    <div class="wi-stats clearfix">
                                        <div class="wis-numbers">
                                            <span><i class="zmdi zmdi-comments"></i> 20</span>
                                            <span class="active"><i class="zmdi zmdi-favorite"></i> 78</span>
                                        </div>

                                        <div class="wis-commentors">
                                            <a href=""><img src="img/profile-pics/2.jpg" alt=""></a>
                                            <a href=""><img src="img/profile-pics/3.jpg" alt=""></a>
                                        </div>
                                    </div>
                                </div>

                                <div class="wi-comments">

                                    <!-- Comment Listing -->
                                    <div class="list-group">
                                        <div class="list-group-item media">
                                            <a href="" class="pull-left"><img src="img/profile-pics/3.jpg" alt="" class="lgi-img"></a>
                                            <div class="media-body">
                                                <a href="" class="lgi-heading">Jonathan Morris <small class="text-muted m-l-10">1 hour ago</small></a>
                                                <small class="lgi-text">Pellentesque mollis fringilla finibus. Class aptent taciti sociosqu ad litora torquent per conubia nostra.</small>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Comment form -->
                                    <div class="wic-form">
                                        <textarea placeholder="Write something..." data-ma-action="wall-comment-open"></textarea>

                                        <div class="wicf-actions text-right">
                                            <button class="btn btn-sm btn-link" data-ma-action="wall-comment-close">Cancel</button>
                                            <button class="btn btn-sm btn-primary">Post</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="card w-item">
                                <div class="card-header">
                                    <div class="media">
                                        <div class="pull-left">
                                            <img class="avatar-img" src="img/profile-pics/5.jpg" alt="">
                                        </div>

                                        <div class="media-body">
                                            <h2>Morgan Francis
                                                <small>Posted on 2nd Sep 2015 at 17:00</small>
                                            </h2>
                                        </div>
                                    </div>
                                </div>

                                <div class="card-body card-padding">
                                    <p>Donec velit libero, gravida vel diam ut, lobortis mollis quam. Morbi posuere egestas posuere. Curabitur in dui sollicitudin, lacinia magna at, laoreet sapien. Integer vitae eros nulla.</p>

                                    <div class="wi-preview lightbox clearfix">
                                        <div class="wip-item" data-src="media/gallery/4.jpg"
                                             style="background-image: url(media/gallery/4.jpg);">
                                            <img src="media/gallery/thumbs/4.jpg" alt="">
                                        </div>
                                        <div class="wip-item" data-src="media/gallery/5.jpg"
                                             style="background-image: url(media/gallery/5.jpg);">
                                            <img src="media/gallery/thumbs/5.jpg" alt="">
                                        </div>
                                        <div class="wip-item" data-src="media/gallery/6.jpg"
                                             style="background-image: url(media/gallery/6.jpg);">
                                            <img src="media/gallery/thumbs/6.jpg" alt="">
                                        </div>
                                    </div>

                                    <div class="wi-stats clearfix">
                                        <div class="wis-numbers">
                                            <span><i class="zmdi zmdi-comments"></i> 20</span>
                                            <span class="active"><i class="zmdi zmdi-favorite"></i> 78</span>
                                        </div>

                                        <div class="wis-commentors">
                                            <a href=""><img src="img/profile-pics/2.jpg" alt=""></a>
                                            <a href=""><img src="img/profile-pics/3.jpg" alt=""></a>
                                        </div>
                                    </div>
                                </div>

                                <div class="wi-comments">

                                    <!-- Comment Listing -->
                                    <div class="list-group">
                                        <div class="list-group-item media">
                                            <a href="" class="pull-left">
                                                <img src="img/profile-pics/3.jpg" alt="" class="lgi-img">
                                            </a>

                                            <div class="media-body">
                                                <a href="" class="lgi-heading">Samantha Diaz <small class="c-gray m-l-10">1 hour ago...</small></a>
                                                <small class="lgi-text">Pellentesque mollis fringilla finibus. Class aptent taciti sociosqu ad litora torquent per conubia nostra.</small>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Comment form -->
                                    <div class="wic-form">
                                        <textarea placeholder="Write something..." data-ma-action="wall-comment-open"></textarea>

                                        <div class="wicf-actions text-right">
                                            <button class="btn btn-sm btn-link" data-ma-action="wall-comment-close">Cancel</button>
                                            <button class="btn btn-sm btn-primary">Post</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="card">
                                <div class="card-header">
                                    <div class="media">
                                        <div class="pull-left">
                                            <img class="avatar-img" src="img/profile-pics/2.jpg" alt="">
                                        </div>

                                        <div class="media-body">
                                            <h2>William Morrison
                                                <small>Posted on 3rd July 2015 at 7:00</small>
                                            </h2>
                                        </div>
                                    </div>
                                </div>

                                <div class="card-body card-padding">
                                    <p>Nullam dignissim, metus et feugiat porttitor.</p>

                                    <div class="wi-preview lightbox clearfix">
                                        <div class="wip-item" data-src="media/gallery/6.jpg"
                                             style="background-image: url(media/gallery/6.jpg);">
                                            <img src="media/gallery/thumbs/6.jpg" alt="">
                                        </div>
                                        <div class="wip-item" data-src="media/gallery/7.jpg"
                                             style="background-image: url(media/gallery/7.jpg);">
                                            <img src="media/gallery/thumbs/7.jpg" alt="">
                                        </div>
                                        <div class="wip-item" data-src="media/gallery/8.jpg"
                                             style="background-image: url(media/gallery/8.jpg);">
                                            <img src="media/gallery/thumbs/8.jpg" alt="">
                                        </div>
                                        <div class="wip-item" data-src="media/gallery/9.jpg"
                                             style="background-image: url(media/gallery/9.jpg);">
                                            <img src="media/gallery/thumbs/5.jpg" alt="">
                                        </div>
                                    </div>

                                    <div class="wi-stats clearfix">
                                        <div class="wis-numbers">
                                            <span><i class="zmdi zmdi-comments"></i> 100</span>
                                            <span class="active"><i class="zmdi zmdi-favorite"></i> 2432</span>
                                        </div>

                                        <div class="wis-commentors">
                                            <a href=""><img src="img/profile-pics/1.jpg" alt=""></a>
                                            <a href=""><img src="img/profile-pics/7.jpg" alt=""></a>
                                            <a href=""><img src="img/profile-pics/9.jpg" alt=""></a>
                                            <a href=""><img src="img/profile-pics/8.jpg" alt=""></a>
                                        </div>
                                    </div>
                                </div>

                                <div class="wi-comments">

                                    <!-- Comment Listing -->
                                    <div class="list-group">
                                        <div class="list-group-item media">
                                            <a href="" class="pull-left">
                                                <img src="img/profile-pics/9.jpg" alt="" class="lgi-img">
                                            </a>

                                            <div class="media-body">
                                                <a href="" class="lgi-heading">Finn Walking <small class="c-gray m-l-10">3rd July 2015</small></a>
                                                <small class="lgi-text">Per conubia nostra.</small>
                                            </div>
                                        </div>
                                        <div class="list-group-item media">
                                            <a href="" class="pull-left">
                                                <img src="img/profile-pics/8.jpg" alt="" class="lgi-img">
                                            </a>

                                            <div class="media-body">
                                                <a href="" class="lgi-heading">Benn Holder <small class="c-gray m-l-10">3rd July 2015</small></a>
                                                <small class="lgi-text">Class aptent taciti sociosqu ad litora torquent per conubia nostra...</small>
                                            </div>
                                        </div>
                                        <div class="list-group-item media">
                                            <a href="" class="pull-left">
                                                <img src="img/profile-pics/3.jpg" alt="" class="lgi-img">
                                            </a>

                                            <div class="media-body">
                                                <a href="" class="lgi-heading">Seam Ford <small class="c-gray m-l-10">3rd July 2015</small></a>
                                                <small class="lgi-text">Praesent bibendum vulputate nulla vitae euismod. Fusce a metus eu ante sagittis viverra sit amet sed odio.</small>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Comment form -->
                                    <div class="wic-form">
                                        <textarea placeholder="Write something..." data-ma-action="wall-comment-open"></textarea>

                                        <div class="wicf-actions text-right">
                                            <button class="btn btn-sm btn-link" data-ma-action="wall-comment-close">Cancel</button>
                                            <button class="btn btn-sm btn-primary">Post</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="card">
                                <div class="card-header">
                                    <div class="media">
                                        <div class="pull-left">
                                            <img class="avatar-img" src="img/profile-pics/7.jpg" alt="">
                                        </div>

                                        <div class="media-body">
                                            <h2>Johnni Schmidt
                                                <small>2nd on Jul 2015 at 02:10</small>
                                            </h2>
                                        </div>
                                    </div>
                                </div>

                                <div class="card-body card-padding">
                                    <p>Morbi non eros nibh. Suspendisse ac nunc faucibus turpis pretium pretium. Nulla metus dui, convallis in lorem id, sodales tincidunt magna....</p>

                                    <div class="wi-preview lightbox clearfix">
                                        <div class="wip-item" data-src="media/gallery/10.jpg"
                                             style="background-image: url(media/gallery/10.jpg);">
                                            <img src="media/gallery/thumbs/10.jpg" alt="">
                                        </div>
                                        <div class="wip-item" data-src="media/gallery/11.jpg"
                                             style="background-image: url(media/gallery/11.jpg);">
                                            <img src="media/gallery/thumbs/11.jpg" alt="">
                                        </div>
                                        <div class="wip-item" data-src="media/gallery/12.jpg"
                                             style="background-image: url(media/gallery/12.jpg);">
                                            <img src="media/gallery/thumbs/12.jpg" alt="">
                                        </div>
                                        <div class="wip-item" data-src="media/gallery/13.jpg"
                                             style="background-image: url(media/gallery/13.jpg);">
                                            <img src="media/gallery/thumbs/13.jpg" alt="">
                                        </div>
                                        <div class="wip-item" data-src="media/gallery/14.jpg"
                                             style="background-image: url(media/gallery/14.jpg);">
                                            <img src="media/gallery/thumbs/14.jpg" alt="">
                                        </div>
                                    </div>

                                    <div class="wi-stats clearfix">
                                        <div class="wis-numbers">
                                            <span><i class="zmdi zmdi-comments"></i> 2</span>
                                            <span><i class="zmdi zmdi-favorite"></i> 10</span>
                                        </div>

                                        <div class="wis-commentors">
                                            <a href=""><img src="img/profile-pics/4.jpg" alt=""></a>
                                            <a href=""><img src="img/profile-pics/8.jpg" alt=""></a>
                                        </div>
                                    </div>
                                </div>

                                <div class="wi-comments">

                                    <!-- Comment Listing -->
                                    <div class="list-group">
                                        <div class="list-group-item media">
                                            <a href="" class="pull-left">
                                                <img src="img/profile-pics/8.jpg" alt="" class="lgi-img">
                                            </a>

                                            <div class="media-body">
                                                <a href="" class="lgi-heading">Jhon Sheamus <small class="c-gray m-l-10">2nd July 2015...</small></a>
                                                <small class="lgi-text">Vivamus vitae sapien et diam convallis hendrerit et eu leo. Nullam dignissim, metus et feugiat porttitor, nulla metus dapibus est, id eleifend nisi massa ac ante.</small>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Comment form -->
                                    <div class="wic-form">
                                        <textarea placeholder="Write something..." data-ma-action="wall-comment-open"></textarea>

                                        <div class="wicf-actions text-right">
                                            <button class="btn btn-sm btn-link" data-ma-action="wall-comment-close">Cancel</button>
                                            <button class="btn btn-sm btn-primary">Post</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="card">
                                <div class="card-header">
                                    <div class="media">
                                        <div class="pull-left">
                                            <img class="avatar-img" src="img/profile-pics/6.jpg" alt="">
                                        </div>

                                        <div class="media-body">
                                            <h2>Sham Alexander
                                                <small>Posted on 30th July 2015 at 7:02</small>
                                            </h2>
                                        </div>
                                    </div>
                                </div>

                                <div class="card-body card-padding">
                                    <p>Praesent sollicitudin venenatis mi, sed consectetur quam accumsan sollicitudin. Aenean ornare tincidunt odio luctus sagittis. Sed vehicula odio eu tortor iaculis, vel lobortis ipsum interdum. Vivamus vitae efficitur elit, in commodo nulla. Fusce ut odio blandit, feugiat mi vitae, pharetra neque. Mauris laoreet commodo augue eget ornare. Vestibulum hendrerit, turpis eget tristique consequat, risus risus dapibus augue, nec commodo nisi nunc mattis dui. Curabitur tempus nibh eget interdum faucibus.</p>

                                    <div class="wi-preview lightbox clearfix">
                                        <div class="wip-item" data-src="media/gallery/15.jpg"
                                             style="background-image: url(media/gallery/15.jpg);">
                                            <img src="media/gallery/thumbs/15.jpg" alt="">
                                        </div>
                                        <div class="wip-item" data-src="media/gallery/16.jpg"
                                             style="background-image: url(media/gallery/16.jpg);">
                                            <img src="media/gallery/thumbs/16.jpg" alt="">
                                        </div>
                                        <div class="wip-item" data-src="media/gallery/17.jpg"
                                             style="background-image: url(media/gallery/17.jpg);">
                                            <img src="media/gallery/thumbs/17.jpg" alt="">
                                        </div>
                                        <div class="wip-item" data-src="media/gallery/18.jpg"
                                             style="background-image: url(media/gallery/18.jpg);">
                                            <img src="media/gallery/thumbs/18.jpg" alt="">
                                        </div>
                                        <div class="wip-item" data-src="media/gallery/19.jpg"
                                             style="background-image: url(media/gallery/19.jpg);">
                                            <img src="media/gallery/thumbs/19.jpg" alt="">
                                        </div>
                                        <div class="wip-item" data-src="media/gallery/20.jpg"
                                             style="background-image: url(media/gallery/20.jpg);">
                                            <img src="media/gallery/thumbs/20.jpg" alt="">
                                        </div>
                                    </div>

                                    <div class="wi-stats clearfix">
                                        <div class="wis-numbers">
                                            <span><i class="zmdi zmdi-comments"></i> 0</span>
                                            <span><i class="zmdi zmdi-favorite"></i> 0</span>
                                        </div>

                                        <div class="wis-commentors">
                                            <a href=""><img src="img/profile-pics/6.jpg" alt=""></a>
                                        </div>
                                    </div>
                                </div>

                                <div class="wi-comments">

                                    <!-- Comment form -->
                                    <div class="wic-form">
                                        <textarea placeholder="Write something..." data-ma-action="wall-comment-open"></textarea>

                                        <div class="wicf-actions text-right">
                                            <button class="btn btn-sm btn-link" data-ma-action="wall-comment-close">Cancel</button>
                                            <button class="btn btn-sm btn-primary">Post</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="card">
                                <div class="card-header">
                                    <div class="media">
                                        <div class="pull-left">
                                            <img class="avatar-img" src="img/profile-pics/5.jpg" alt="">
                                        </div>

                                        <div class="media-body">
                                            <h2>Frances Gonzales
                                                <small>Posted on 25th July 2015 at 03:12</small>
                                            </h2>
                                        </div>
                                    </div>
                                </div>

                                <div class="card-body card-padding">
                                    <p>Suspendisse dapibus ante ex, non tempor ligula molestie nec. In nisl dui, rutrum in libero id, condimentum fermentum velit. Praesent magna magna, aliquam ut purus sit amet, fringilla placerat ipsum</p>

                                    <div class="wi-preview lightbox clearfix">
                                        <div class="wip-item" data-src="media/gallery/21.jpg"
                                             style="background-image: url(media/gallery/21.jpg);">
                                            <img src="media/gallery/thumbs/21.jpg" alt="">
                                        </div>
                                        <div class="wip-item" data-src="media/gallery/22.jpg"
                                             style="background-image: url(media/gallery/22.jpg);">
                                            <img src="media/gallery/thumbs/22.jpg" alt="">
                                        </div>
                                        <div class="wip-item" data-src="media/gallery/23.jpg"
                                             style="background-image: url(media/gallery/23.jpg);">
                                            <img src="media/gallery/thumbs/23.jpg" alt="">
                                        </div>
                                        <div class="wip-item" data-src="media/gallery/24.jpg"
                                             style="background-image: url(media/gallery/24.jpg);">
                                            <img src="media/gallery/thumbs/24.jpg" alt="">
                                        </div>
                                        <div class="wip-item" data-src="media/gallery/5.jpg"
                                             style="background-image: url(media/gallery/5.jpg);">
                                            <img src="media/gallery/thumbs/5.jpg" alt="">
                                        </div>
                                        <div class="wip-item" data-src="media/gallery/6.jpg"
                                             style="background-image: url(media/gallery/6.jpg);">
                                            <img src="media/gallery/thumbs/6.jpg" alt="">
                                        </div>
                                        <div class="wip-item" data-src="media/gallery/7.jpg"
                                             style="background-image: url(media/gallery/7.jpg);">
                                            <img src="media/gallery/thumbs/7.jpg" alt="">
                                        </div>
                                    </div>

                                    <div class="wi-stats clearfix">
                                        <div class="wis-numbers">
                                            <span><i class="zmdi zmdi-comments"></i> 115</span>
                                            <span class="active"><i class="zmdi zmdi-favorite"></i> 1265</span>
                                        </div>

                                        <div class="wis-commentors">
                                            <a href=""><img src="img/profile-pics/2.jpg" alt=""></a>
                                            <a href=""><img src="img/profile-pics/3.jpg" alt=""></a>
                                            <a href=""><img src="img/profile-pics/1.jpg" alt=""></a>
                                            <a href=""><img src="img/profile-pics/6.jpg" alt=""></a>
                                            <a href=""><img src="img/profile-pics/4.jpg" alt=""></a>
                                        </div>
                                    </div>
                                </div>

                                <div class="wi-comments">

                                    <!-- Comment Listing -->
                                    <div class="list-group">
                                        <div class="list-group-item media">
                                            <a href="" class="pull-left">
                                                <img src="img/profile-pics/2.jpg" alt="" class="lgi-img">
                                            </a>

                                            <div class="media-body">
                                                <a href="" class="lgi-heading">Stacey Hunt <small class="c-gray m-l-10">30th July 2015</small></a>
                                                <small class="lgi-text">Mauris elit metus, scelerisque sit amet est sit amet, scelerisque aliquam arcu. Nam sollicitudin risus ipsum.</small>
                                            </div>
                                        </div>

                                        <div class="list-group-item media">
                                            <a href="" class="pull-left">
                                                <img src="img/profile-pics/4.jpg" alt="" class="lgi-img">
                                            </a>

                                            <div class="media-body">
                                                <a href="" class="lgi-heading">Frederick Taylor <small class="c-gray m-l-10">29th July 2015</small></a>
                                                <small class="lgi-text">Nunc id blandit libero.</small>
                                            </div>
                                        </div>

                                        <div class="list-group-item media">
                                            <a href="" class="pull-left">
                                                <img src="img/profile-pics/3.jpg" alt="" class="lgi-img">
                                            </a>

                                            <div class="media-body">
                                                <a href="" class="lgi-heading">Belkin Raed <small class="c-gray m-l-10">27th July 2015</small></a>
                                                <small class="lgi-text">Nam sollicitudin risus ipsum. Ut scelerisque ac odio eu efficitur. Vestibulum a dolor interdum, molestie ipsum sed, feugiat nisi. Nunc ut consequat est, nec pharetra augue</small>
                                            </div>
                                        </div>

                                        <div class="list-group-item media">
                                            <a href="" class="pull-left">
                                                <img src="img/profile-pics/6.jpg" alt="" class="lgi-img">
                                            </a>

                                            <div class="media-body">
                                                <a href="" class="lgi-heading">James Simmons <small class="c-gray m-l-10">23rd July 2015</small></a>
                                                <small class="lgi-text">Ha ha....</small>
                                            </div>
                                        </div>

                                        <div class="list-group-item media">
                                            <a href="" class="pull-left">
                                                <img src="img/profile-pics/3.jpg" alt="" class="lgi-img">
                                            </a>

                                            <div class="media-body">
                                                <a href="" class="lgi-heading">Wendy Macshaw <small class="c-gray m-l-10">20th July 2015</small></a>
                                                <small class="lgi-text">Praesent sollicitudin venenatis mi, sed consectetur quam accumsan sollicitudin. Aenean ornare tincidunt odio luctus sagittis. Sed vehicula odio eu tortor iaculis, vel lobortis ipsum interdum.</small>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Comment form -->
                                    <div class="wic-form">
                                        <textarea placeholder="Write something..." data-ma-action="wall-comment-open"></textarea>

                                        <div class="wicf-actions text-right">
                                            <button class="btn btn-sm btn-link" data-ma-action="wall-comment-close">Cancel</button>
                                            <button class="btn btn-sm btn-primary">Post</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="card">
                                <div class="card-header">
                                    <div class="media">
                                        <div class="pull-left">
                                            <img class="avatar-img" src="img/profile-pics/2.jpg" alt="">
                                        </div>

                                        <div class="media-body">
                                            <h2>Shane Wtson
                                                <small>Posted on 20th July 2015 at 7:00</small>
                                            </h2>
                                        </div>
                                    </div>
                                </div>

                                <div class="card-body card-padding">
                                    <p>Donec a ipsum eu tellus auctor sodales. Nulla facilisi. Praesent et ex finibus, tempor arcu interdum, facilisis mi. Nunc blandit hendrerit nulla, sed rutrum risus. Nulla eu mollis massa, et laoreet nulla. Ut ut dolor et arcu eleifend elementum ut eget tellus. Vestibulum eu hendrerit mauris. Suspendisse id tortor vel nisl tincidunt interdum.</p>

                                    <div class="wi-preview lightbox clearfix">
                                        <div class="wip-item" data-src="media/gallery/6.jpg"
                                             style="background-image: url(media/gallery/6.jpg);">
                                            <img src="media/gallery/thumbs/6.jpg" alt="">
                                        </div>
                                        <div class="wip-item" data-src="media/gallery/7.jpg"
                                             style="background-image: url(media/gallery/7.jpg);">
                                            <img src="media/gallery/thumbs/7.jpg" alt="">
                                        </div>
                                        <div class="wip-item" data-src="media/gallery/8.jpg"
                                             style="background-image: url(media/gallery/8.jpg);">
                                            <img src="media/gallery/thumbs/8.jpg" alt="">
                                        </div>
                                        <div class="wip-item" data-src="media/gallery/9.jpg"
                                             style="background-image: url(media/gallery/9.jpg);">
                                            <img src="media/gallery/thumbs/9.jpg" alt="">
                                        </div>
                                        <div class="wip-item" data-src="media/gallery/10.jpg"
                                             style="background-image: url(media/gallery/10.jpg);">
                                            <img src="media/gallery/thumbs/10.jpg" alt="">
                                        </div>
                                        <div class="wip-item" data-src="media/gallery/11.jpg"
                                             style="background-image: url(media/gallery/11.jpg);">
                                            <img src="media/gallery/thumbs/11.jpg" alt="">
                                        </div>
                                        <div class="wip-item" data-src="media/gallery/12.jpg"
                                             style="background-image: url(media/gallery/12.jpg);">
                                            <img src="media/gallery/thumbs/12.jpg" alt="">
                                        </div>
                                        <div class="wip-item" data-src="media/gallery/13.jpg"
                                             style="background-image: url(media/gallery/13.jpg);">
                                            <img src="media/gallery/thumbs/13.jpg" alt="">
                                        </div>

                                    </div>

                                    <div class="wi-stats clearfix">
                                        <div class="wis-numbers">
                                            <span><i class="zmdi zmdi-comments"></i> 100</span>
                                            <span class="active"><i class="zmdi zmdi-favorite"></i> 2432</span>
                                        </div>

                                        <div class="wis-commentors">
                                            <a href=""><img src="img/profile-pics/1.jpg" alt=""></a>
                                            <a href=""><img src="img/profile-pics/7.jpg" alt=""></a>
                                            <a href=""><img src="img/profile-pics/9.jpg" alt=""></a>
                                            <a href=""><img src="img/profile-pics/8.jpg" alt=""></a>
                                        </div>
                                    </div>
                                </div>

                                <div class="wi-comments">

                                    <!-- Comment Listing -->
                                    <div class="list-group">
                                        <div class="list-group-item media">
                                            <a href="" class="pull-left">
                                                <img src="img/profile-pics/9.jpg" alt="" class="lgi-img">
                                            </a>

                                            <div class="media-body">
                                                <a href="" class="lgi-heading">Finn Walking <small class="c-gray m-l-10">3rd July 2015</small></a>
                                                <small class="lgi-text">Per conubia nostra.</small>
                                            </div>
                                        </div>
                                        <div class="list-group-item media">
                                            <a href="" class="pull-left">
                                                <img src="img/profile-pics/8.jpg" alt="" class="lgi-img">
                                            </a>

                                            <div class="media-body">
                                                <a href="" class="lgi-heading">Benn Holder <small class="c-gray m-l-10">3rd July 2015</small></a>
                                                <small class="lgi-text">Class aptent taciti sociosqu ad litora torquent per conubia nostra...</small>
                                            </div>
                                        </div>
                                        <div class="list-group-item media">
                                            <a href="" class="pull-left">
                                                <img src="img/profile-pics/3.jpg" alt="" class="lgi-img">
                                            </a>

                                            <div class="media-body">
                                                <a href="" class="lgi-heading">Seam Ford <small class="c-gray m-l-10">3rd July 2015</small></a>
                                                <small class="lgi-text">Praesent bibendum vulputate nulla vitae euismod. Fusce a metus eu ante sagittis viverra sit amet sed odio.</small>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Comment form -->
                                    <div class="wic-form">
                                        <textarea placeholder="Write something..." data-ma-action="wall-comment-open"></textarea>

                                        <div class="wicf-actions text-right">
                                            <button class="btn btn-sm btn-link" data-ma-action="wall-comment-close">Cancel</button>
                                            <button class="btn btn-sm btn-primary">Post</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 hidden-sm hidden-xs">
                            <div class="card">
                                <div class="card-header">
                                    <h2>About me</h2>
                                </div>

                                <div class="card-body card-padding">
                                    Maecenas malesuada. Nam adipiscing. Etiam vitae tortor. Maecenas ullamcorper, dui et
                                    placerat feugiat, eros pede varius nisi, condimentum viverra felis nunc et lorem. <a
                                        data-ui-sref="pages.profile.profile-about">
                                    <small>Read more...</small>
                                </a>
                                </div>
                            </div>

                            <div class="card picture-list">
                                <div class="card-header">
                                    <h2>Photos
                                        <small>Cras congue nec lorem eget posuere</small>
                                    </h2>

                                    <ul class="actions">
                                        <li class="dropdown action-show">
                                            <a href="">
                                                <i class="zmdi zmdi-more-vert"></i>
                                            </a>

                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li>
                                                    <a href="">Refresh</a>
                                                </li>
                                                <li>
                                                    <a href="">Manage Widgets</a>
                                                </li>
                                                <li>
                                                    <a href="">Widgets Settings</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>

                                <div class="pl-body">
                                    <div class="col-xs-3">
                                        <a href="">
                                            <img src="img/headers/square/1.png" alt="">
                                        </a>
                                    </div>

                                    <div class="col-xs-3">
                                        <a href="">
                                            <img src="img/headers/square/2.png" alt="">
                                        </a>
                                    </div>
                                    <div class="col-xs-3">
                                        <a href="">
                                            <img src="img/headers/square/3.png" alt="">
                                        </a>
                                    </div>
                                    <div class="col-xs-3">
                                        <a href="">
                                            <img src="img/headers/square/4.png" alt="">
                                        </a>
                                    </div>
                                    <div class="col-xs-3">
                                        <a href="">
                                            <img src="img/headers/square/5.png" alt="">
                                        </a>
                                    </div>
                                    <div class="col-xs-3">
                                        <a href="">
                                            <img src="img/headers/square/6.png" alt="">
                                        </a>
                                    </div>
                                    <div class="col-xs-3">
                                        <a href="">
                                            <img src="img/headers/square/7.png" alt="">
                                        </a>
                                    </div>
                                    <div class="col-xs-3">
                                        <a href="">
                                            <img src="img/headers/square/8.png" alt="">
                                        </a>
                                    </div>
                                    <div class="col-xs-3">
                                        <a href="">
                                            <img src="img/headers/square/9.png" alt="">
                                        </a>
                                    </div>
                                    <div class="col-xs-3">
                                        <a href="">
                                            <img src="img/headers/square/1.png" alt="">
                                        </a>
                                    </div>
                                    <div class="col-xs-3">
                                        <a href="">
                                            <img src="img/headers/square/2.png" alt="">
                                        </a>
                                    </div>
                                    <div class="col-xs-3">
                                        <a href="">
                                            <img src="img/headers/square/3.png" alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="card">
                                <div class="card-header">
                                    <h2>Recent Comments
                                        <small>Commodo vel pharetra nisi. Donec velit libero</small>
                                    </h2>
                                </div>

                                <div class="list-group">
                                    <a class="list-group-item media" href="">
                                        <div class="pull-left">
                                            <img class="lgi-img" src="img/profile-pics/1.jpg" alt="">
                                        </div>
                                        <div class="media-body">
                                            <div class="lgi-heading">David Belle</div>
                                            <small class="lgi-text">Cum sociis natoque penatibus et magnis dis parturient montes</small>
                                        </div>
                                    </a>
                                    <a class="list-group-item media" href="">
                                        <div class="pull-left">
                                            <img class="lgi-img" src="img/profile-pics/2.jpg" alt="">
                                        </div>
                                        <div class="media-body">
                                            <div class="lgi-heading">Jonathan Morris</div>
                                            <small class="lgi-text">Nunc quis diam diamurabitur at dolor elementum, dictum turpis vel</small>
                                        </div>
                                    </a>
                                    <a class="list-group-item media" href="">
                                        <div class="pull-left">
                                            <img class="lgi-img" src="img/profile-pics/3.jpg" alt="">
                                        </div>
                                        <div class="media-body">
                                            <div class="lgi-heading">Fredric Mitchell Jr.</div>
                                            <small class="lgi-text">Phasellus a ante et est ornare accumsan at vel magnauis blandit turpis at augue ultricies</small>
                                        </div>
                                    </a>
                                    <a class="list-group-item media" href="">
                                        <div class="pull-left">
                                            <img class="lgi-img" src="img/profile-pics/4.jpg" alt="">
                                        </div>
                                        <div class="media-body">
                                            <div class="lgi-heading">Glenn Jecobs</div>
                                            <small class="lgi-text">Ut vitae lacus sem ellentesque maximus, nunc sit amet varius dignissim, dui est consectetur neque</small>
                                        </div>
                                    </a>
                                    <a class="list-group-item media" href="">
                                        <div class="pull-left">
                                            <img class="lgi-img" src="img/profile-pics/4.jpg" alt="">
                                        </div>
                                        <div class="media-body">
                                            <div class="lgi-heading">Bill Phillips</div>
                                            <small class="lgi-text">Proin laoreet commodo eros id faucibus. Donec ligula quam, imperdiet vel ante placerat</small>
                                        </div>
                                    </a>
                                    <a class="view-more" href="">View All</a>
                                </div>
                            </div>

                            <div class="card">
                                <div class="card-header">
                                    <h2>Contact Information
                                        <small>Fusce eget dolor id justo luctus commodo vel pharetra nisi. Donec velit
                                            libero
                                        </small>
                                    </h2>
                                </div>
                                <div class="card-body card-padding">
                                    <div class="pmo-contact">
                                        <ul>
                                            <li class="ng-binding"><i class="zmdi zmdi-phone"></i> 00971123456789</li>
                                            <li class="ng-binding"><i class="zmdi zmdi-email"></i> malinda.h@gmail.com
                                            </li>
                                            <li class="ng-binding"><i class="zmdi zmdi-facebook-box"></i>
                                                malinda.hollaway
                                            </li>
                                            <li class="ng-binding"><i class="zmdi zmdi-twitter"></i> @malinda
                                                (twitter.com/malinda)
                                            </li>
                                            <li>
                                                <i class="zmdi zmdi-pin"></i>
                                                <address class="m-b-0 ng-binding">
                                                    44-46 Morningside Road,<br>
                                                    Edinburgh,<br>
                                                    Scotland
                                                </address>
                                            </li>
                                        </ul>
                                    </div>

                                    <a class="pmo-map" href="">
                                        <img src="img/demo/map.png" alt="">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </section>
        
        <footer id="footer">
            Copyright &copy; 2015 Material Admin
            
            <ul class="f-menu">
                <li><a href="">Home</a></li>
                <li><a href="">Dashboard</a></li>
                <li><a href="">Reports</a></li>
                <li><a href="">Support</a></li>
                <li><a href="">Contact</a></li>
            </ul>
        </footer>

        <!-- Page Loader -->
        <div class="page-loader">
            <div class="preloader pls-blue">
                <svg class="pl-circular" viewBox="25 25 50 50">
                    <circle class="plc-path" cx="50" cy="50" r="20" />
                </svg>

                <p>Please wait...</p>
            </div>
        </div>
    
        <!-- Older IE warning message -->
        <!--[if lt IE 9]>
            <div class="ie-warning">
                <h1 class="c-white">Warning!!</h1>
                <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers to access this website.</p>
                <div class="iew-container">
                    <ul class="iew-download">
                        <li>
                            <a href="http://www.google.com/chrome/">
                                <img src="img/browsers/chrome.png" alt="">
                                <div>Chrome</div>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.mozilla.org/en-US/firefox/new/">
                                <img src="img/browsers/firefox.png" alt="">
                                <div>Firefox</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://www.opera.com">
                                <img src="img/browsers/opera.png" alt="">
                                <div>Opera</div>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.apple.com/safari/">
                                <img src="img/browsers/safari.png" alt="">
                                <div>Safari</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                                <img src="img/browsers/ie.png" alt="">
                                <div>IE (New)</div>
                            </a>
                        </li>
                    </ul>
                </div>
                <p>Sorry for the inconvenience!</p>
            </div>   
        <![endif]-->
</home>
@endsection
