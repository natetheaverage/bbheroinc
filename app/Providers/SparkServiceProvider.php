<?php

namespace App\Providers;

use Laravel\Spark\Spark;
use Laravel\Spark\Providers\AppServiceProvider as ServiceProvider;

class SparkServiceProvider extends ServiceProvider
{
    /**
     * Your application and company details.
     *
     * @var array
     */
    protected $details = [
        'vendor' => 'BB Hero Inc',
        'product' => 'SuperHero Training',
        'street' => 'TBA',
        'location' => 'Denver, Co. 80203',
        'phone' => '555-555-5555',
    ];

    /**
     * The address where customer support e-mails should be sent.
     *
     * @var string
     */
    protected $sendSupportEmailsTo = null;

    /**
     * All of the application developer e-mail addresses.
     *
     * @var array
     */
    protected $developers = [
        "natetheaverage@gmail.com",
        //
    ];

    /**
     * Indicates if the application will expose an API.
     *
     * @var bool
     */
    protected $usesApi = true;

    /**
     * Finish configuring Spark for the application.
     *
     * @return void
     */
    public function booted()
    {
        Spark::useStripe()->noCardUpFront()->trialDays(3);

        Spark::freePlan()
            ->features([
                'Intro Course', 'Teasers', 'Something else'
            ]);

        Spark::plan('Basic', 'stripe-test-1')
            ->price(35)
            ->features([
                'One Corse', 'Community Forum', 'Something else'
            ]);

        Spark::plan('Basic', 'stripe-test-2')
            ->price(60)
            ->features([
                'All Five Corses', 'Community Forum', 'Something else'
            ]);
    }
}
